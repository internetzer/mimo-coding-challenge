import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { LessonDetailComponent } from './lessons/lesson-detail/lesson-detail.component';
import { LessonsCompletedComponent } from './lessons/lessons-completed/lessons-completed.component';
import { LessonsStartComponent } from './lessons/lessons-start/lessons-start.component';

@NgModule({
  declarations: [
    AppComponent,
    LessonDetailComponent,
    LessonsCompletedComponent,
    LessonsStartComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
