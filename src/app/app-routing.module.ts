import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LessonsStartComponent} from './lessons/lessons-start/lessons-start.component';
import { LessonDetailComponent }      from './lessons/lesson-detail/lesson-detail.component';
import { LessonsCompletedComponent} from './lessons/lessons-completed/lessons-completed.component';

const routes: Routes = [
  { path: '', redirectTo: '/start', pathMatch: 'full' },
  { path: 'start', component: LessonsStartComponent  },
  { path: 'lessons/:id', component: LessonDetailComponent },
  { path: 'completed', component: LessonsCompletedComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
