import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LessonsService } from '../lessons.service';
import { Lesson, LessonContent } from '../lessons.model';

@Component({
  selector: 'app-lesson-detail',
  templateUrl: './lesson-detail.component.html',
  styleUrls: ['./lesson-detail.component.css']
})
export class LessonDetailComponent implements OnInit {
  
  lesson: Lesson;
  contents: LessonContent[];
  answer: string;
  match: string;
  solved: boolean;
  lessonStarted: string;
  lessonFinished: string;

  constructor(private route: ActivatedRoute,private lessonsService: LessonsService) {}

  async ngOnInit() {
    this.route.params.subscribe(params => {
      this.loadLesson(+params["id"]);
   });
  }

  answerChanged(): void {
    this.solved = this.answer === this.match;
  }

  async lessonCompleted(): Promise<void> {
    let lessonCompletionEvent = {
      id: this.lesson.id,
      lessonStarted: this.lessonStarted,
      lessonFinished: this.lessonFinished = new Date().toISOString()
    }
    this.lessonsService.saveLessonCompletionEvent(lessonCompletionEvent);
    this.lessonsService.loadNextLesson(this.lesson.id);
  }

  private async loadLesson(id: number) {
    this.solved = false;
    this.contents = new Array<LessonContent>();
    this.lesson = await this.lessonsService.getLesson(id);

    if(this.lesson) {
      let idx = 0;
      
      if(this.lesson.input) {
        this.loadMatchText();
      }
      else {
        this.solved = true;
      }
      
      this.lesson.content.forEach((content) => {
        const blockStart = idx +1;
        const blockEnd = idx + content.text.length;
        content.type = "output";

        if(this.contentBlockHasInput(blockStart, blockEnd)) {
          this.addContentBlocks(content, blockStart, blockEnd);
        }
        else {
          this.contents.push(content);
        }
        idx += content.text.length -1;
      });
      this.lessonStarted = new Date().toISOString();
    }
  }

  private contentBlockHasInput(blockStart: number, blockEnd: number): boolean {
    return this.lesson.input && 
           this.lesson.input.startIndex >= blockStart &&
           this.lesson.input.endIndex <= blockEnd
  }

  private addContentBlocks(content: LessonContent, blockStart: number, blockEnd: number): void {
    // add additional output block if necessary
    if(this.lesson.input.startIndex > blockStart)
    {
      this.contents.push(this.createLessonContent(content.color, "output", content.text.substring(0,this.lesson.input.startIndex - blockStart)));
    }

    // add input block
    this.contents.push(this.createLessonContent(content.color, "input", "", (this.lesson.input.endIndex - this.lesson.input.startIndex) * 10 + "px"));

    // add additional output block if necessary
    if(this.lesson.input.endIndex < blockEnd)
    {
      this.contents.push(this.createLessonContent(content.color, "output", content.text.substring(this.lesson.input.endIndex - this.lesson.input.startIndex)));
    }
  }

  private loadMatchText(): void {
    const content = this.lesson.content.map(c => c.text).join("");
    this.match = content.substring(this.lesson.input.startIndex, this.lesson.input.endIndex);
  }

  private createLessonContent(color: string, type: string, text: string, width?:string): LessonContent {
    let content = new LessonContent();
    content.color = color;
    content.type = type;
    content.text = text;
    content.width = width;
    return content;
  }
}
