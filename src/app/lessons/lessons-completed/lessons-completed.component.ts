import { Component, OnInit } from '@angular/core';
import { LessonsService } from '../lessons.service';

@Component({
  selector: 'app-lessons-completed',
  templateUrl: './lessons-completed.component.html',
  styleUrls: ['./lessons-completed.component.css']
})
export class LessonsCompletedComponent implements OnInit {

  constructor(private lessonsService: LessonsService) { }

  ngOnInit() {
  }

  startLessons(): void {
    this.lessonsService.startLessons();
  }
}
