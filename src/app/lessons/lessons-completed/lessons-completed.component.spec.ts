import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LessonsCompletedComponent } from './lessons-completed.component';

describe('LessonsCompletedComponent', () => {
  let component: LessonsCompletedComponent;
  let fixture: ComponentFixture<LessonsCompletedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LessonsCompletedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LessonsCompletedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
