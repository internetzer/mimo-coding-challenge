import { Component, OnInit } from '@angular/core';
import { LessonsService } from '../lessons.service';

@Component({
  selector: 'app-lessons-start',
  templateUrl: './lessons-start.component.html',
  styleUrls: ['./lessons-start.component.css']
})
export class LessonsStartComponent implements OnInit {

  constructor(private lessonsService: LessonsService) { }

  ngOnInit() {
  }

  startLessons(): void {
    this.lessonsService.startLessons();
  }

}
