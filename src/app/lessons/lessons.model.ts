export class Lesson {
    id: number;
    content: LessonContent[];
    input: LessonInput;
  }

export class LessonContent {
    color: string;
    text: string;
    type: string
    width: string;
}

export class LessonInput {
    startIndex: number;
    endIndex: number;
}