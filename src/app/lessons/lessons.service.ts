import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Lesson } from './lessons.model';

@Injectable({
  providedIn: 'root'
})
export class LessonsService {

  private loadedLessons: Lesson[];

  constructor(private router: Router, private route: ActivatedRoute, private http: HttpClient) { }

  public async getLessons(): Promise<Lesson[]> {
    if(!this.loadedLessons) {
      const getLessonsResult = await this.http.get("https://file-bzxjxfhcyh.now.sh/").toPromise();
      this.loadedLessons = getLessonsResult["lessons"];
    }
    return this.loadedLessons;
  }

  public async getLesson(id: number): Promise<Lesson> {
    const lessons = await this.getLessons();
    return lessons.find(l => l.id === id);
  }

  public async startLessons(): Promise<void> {
    const lessons = await this.getLessons();
    this.router.navigate(['../../lessons', lessons[0].id], {relativeTo: this.route});
  }

  public async loadNextLesson(id: number): Promise<void> {
    const lessons = await this.getLessons();
    const nextLesson = lessons.find(l => l.id === id+1);
    
    if(nextLesson) {
      this.router.navigate(['../../lessons', nextLesson.id], {relativeTo: this.route});
    }
    else {
      this.router.navigate(['../../completed'], {relativeTo: this.route});
    }
  }

  public saveLessonCompletionEvent(lessonCompletionEvent): void {
    let completionEvents = localStorage.getItem("lessonCompletionEvents");
    if(completionEvents) {
      const completionEventsData = JSON.parse(completionEvents);
      completionEventsData.push(lessonCompletionEvent);
      completionEvents = JSON.stringify(completionEventsData);
    }
    else {
      completionEvents = JSON.stringify([lessonCompletionEvent]);
    }
    localStorage.setItem("lessonCompletionEvents", completionEvents);
  }
}